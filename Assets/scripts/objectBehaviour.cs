﻿using UnityEngine;
using System.Collections;

public class objectBehaviour : MonoBehaviour {
	//velocidade da movimentacao
	public float moveSpeed = 0.2f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		moveObject ();


	}

	public void moveObject(){
		if(Input.GetKey("left"))
		{
			var pos = transform.position;
			pos.x -= 0.2f;
			transform.position = pos;
		}

		if(Input.GetKey("right"))
		{
			var pos = transform.position;
			pos.x += 0.2f;
			transform.position = pos;
		}

		if(Input.GetKey("up"))
		{
			var pos = transform.position;
			pos.z += 0.2f;
			transform.position = pos;
		}

		if(Input.GetKey("down"))
		{
			var pos = transform.position;
			pos.z -= 0.2f;
			transform.position = pos;
		}

	}
}
